<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Para tener acceso a los post de eloquent(mi tabla de mi bd).
use App\Post;



// Route::get('eloquent', function () {
//     $posts = Post::where('id', '>=', '20')
//     ->orderBy('id', 'desc')
//     ->take(3)
//     ->get();

//     foreach($posts as $post) {
//         echo "$post->id $post->title <br>";
//     }
// });


//Relaciones con laravel.
Route::get('eloquent', function () {
    $posts = Post::where('id', '>=', '20')
    ->orderBy('id', 'desc')
    ->take(3)
    ->get();

    foreach($posts as $post) {
        echo "$post->id $post->title <br>";
    }
});

//Relaciones: muestra al usuario y al titulo que agrego
Route::get('posts', function () {
    $posts = Post::get();

    foreach($posts as $post) {
         /*como estamos usando varios niveles,
        lo encerramos dentro de llaves
        el siguiente campo:
        */
        echo "
        $post->id 
        <strong>{$post->user->get_name}</strong>
        $post->get_title 
        <br>";
    }
});

//Relaciones: cuantos posts tienen esos usuarios.
use App\User;
Route::get('users', function () {
    $users = User::all();//puede ser get() o bien all()

    foreach($users as $user) {
        echo "
        $user->id 
        <strong>{$user->get_name}</strong>
        {$user->posts->count()} <br>
        ";
        /*
        el campo de posts viene del metodo que esta donde se hereda y así lo declaramos 
        y en count es un método para contar cuántas cosas o registros hay en una tabla.
        */
    }
});



//serializacion y colecciones
Route::get('collections', function () {
    $users = User::all();
    //para ver el objeto completo.
    //dd($users);
    //mostrar el numero 5 si es que existe.
    //dd($users->contains(5));
    //todos los usuarios excepto los que yo configure a continuación
    //dd($users->except([1, 2, 3]));
    //solamente muestra el que estoy indicando que es el 4
    //dd($users->only(4));
    //buscar un único elemento que te estoy diciendo
    //dd($users->find(4));
    //traeme los usuarios cargando la relación con post.
    dd($users->load('posts'));
});



//serializacion y colecciones
Route::get('serialization', function () {
    //Para traer unos datos
    /*$users = User::all();
    // 
    $user = $users->find(1);
    dd($user);*/
    //para traer otros datos para retornar un JSON.
    $users = User::all();
    // 
    $user = $users->find(1);
    dd($user->toJson());
});