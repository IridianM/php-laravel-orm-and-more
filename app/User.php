<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts() {
        //un usuario puede tener muchos posts
        return $this->hasMany(Post::class);
    }
    //Este método debe comenzar en get y terminar con la palabra attribute.
    //lo que va a ir en medio va a ser el nombre de nuestro campo lógico.
    //después se tiene que usar este metidi en las rutas.
    public function getGetNameAttribute() 
    {
        return strtoupper ($this->name);
    }
    //Para guardarlos en la base de datos con el siguiente formato., 
    ///sin agregar ningún prefijo  por que simplemente quiero que se mantenga 
    //el nombre del campo.
    public function setNameAttribute ($value) 
    {
        $this->attributes['name'] = strtolower($value);
    }
}
