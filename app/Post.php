<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    
    public function user() 
    {
        //un post pertenece a un usuario
        return $this->belongsTo(User::class);
    }
    //Mostrar en mayusculas los datos.
    public function getGetTitleAttribute() 
    {
        //return strtoupper ($this->title);
        return ucfirst ($this->title);
    }

    //Para guardar en la base de datos.
    public function setTitleAttribute ($value) 
    {
        $this->attributes['title'] = strtolower($value);
    }
}
