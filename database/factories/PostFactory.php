<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        //Queremos que se cree y se rellene el campo de usuarios
        //aleatoriamente del 1 al 4 por que son los usuarios que tenemos en la base de datos.
        'user_id' => rand(1, 4),
        'title' => $faker->sentence
    ];
});
